#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

using namespace cv;
using namespace std;

void MainWindow::on_pushButton_clicked()
{
    Mat src;

    src = imread("C:/Data/EVDML_Project/markPatternHighRes.png",IMREAD_COLOR);
    if(!src.data) {
       ui->statusbar->showMessage(QString("Could not open image!"),0);
    }
    else {

       int height = src.rows, width = src.cols;
       QString info;

       // Get the image data
       info.append(QString("Image info: "));
       info.append(QString("height=%1 ").arg(height));
       info.append(QString("width=%1 ").arg(width));

       ui->statusbar->showMessage(info);

       // Create a window
       namedWindow("Original image", WINDOW_AUTOSIZE);
       moveWindow("Original image", 50,-100);
       // Show the image
       imshow("Original image",src);
       waitKey(0);

       //Convert to grayscale
       Mat gray(width,height,CV_8UC1,1);
       cvtColor(src,gray,COLOR_BGR2GRAY);
       // Create a window
       namedWindow("Grayscale", WINDOW_AUTOSIZE);
       moveWindow("Grayscale", 50, 250);
       // Show the image
       imshow("Grayscale",gray);
       waitKey(0);

       //Edge detection Canny
       Mat edge(width,height,CV_8UC1,1);
       Canny(gray,edge,30,150);
       // Create a window
       namedWindow("Edgedetection Canny", WINDOW_AUTOSIZE);
       moveWindow("Edgedetection Canny", 50, 600);
       // Show the image
       imshow("Edgedetection Canny",edge);
       waitKey(0);

       //Create binary image
       Mat binary(width,height,CV_8UC1,1);

       threshold(gray,binary,235,255,THRESH_BINARY_INV);
       // Create a window
       namedWindow("Binary", WINDOW_AUTOSIZE);
       moveWindow("Binary", 50, 150);
       // Show the image
       imshow("Binary",binary);
       waitKey(0);

       //Find contours
       Mat contourImage(binary.size(),CV_8UC3,Scalar(0,0,0));
       vector<vector<Point>> contours;
       vector<Vec4i> hierarchy;
       Point offset;
       Scalar colors[3];
           colors[0] = cv::Scalar(255, 0, 0);
           colors[1] = cv::Scalar(0, 255, 0);
           colors[2] = cv::Scalar(0, 0, 255);
       findContours(binary,contours,hierarchy,RETR_CCOMP,CHAIN_APPROX_NONE,offset);

       // Approximate contours to polygons + get bounding rects and circles
       //Draw contours and output image
       vector<vector<Point> > contours_poly( contours.size() );
       vector<Rect> boundRect( contours.size() );
       vector<Point2f>center( contours.size() );
       vector<float>radius( contours.size() );

       for( size_t i = 0; i < contours.size(); i++ ){
           approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
           boundRect[i] = boundingRect( Mat(contours_poly[i]) );
           minEnclosingCircle( contours_poly[i], center[i], radius[i] );
           drawContours(contourImage, contours, i, colors[i % 3]);
       }

       // Draw polygonal contour, bonding rects or circles
       Mat drawing = Mat::zeros( binary.size(), CV_8UC3 );
       for( size_t i = 0; i< contours.size(); i++ ){
            // use a random color:     Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            Scalar color = Scalar( 0, 0, 240 );

            // draw contours:     drawContours( drawing, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
            // draw rectangle:    rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );
            // draw circle with dynamic radious:      circle( drawing, center[i], (int)radius[i], color, 2, 8, 0 );
            circle( drawing, center[i], 2, color, 2, 8, 0 );

            ostringstream ss;
            ss << flush << "id:" << i << ",x:" << center[i].x << ",y: " << center[i].y;
            //print the id number of the countour in the image
            putText(drawing,ss.str(),(center[i] + Point2f(5,5)),FONT_ITALIC,0.5,color);
            // print data sctructure to command line to be used in another program etc
            cout << "{id:" << i << ",x:" << center[i].x << ",y:" << center[i].y << "}," << endl;
          }

       // print some clear lines to command line to seperate output from different runs
       cout << "\n \n" << flush;

       //add centerpoints to contour image
       Mat result = Mat::zeros(binary.size(),CV_8UC3);
       add(contourImage,drawing,result);

       // Create a window
       namedWindow("Contours", WINDOW_AUTOSIZE);
       moveWindow("Contours", 50, 500);
       // Show the image
       imshow("Contours",result);
       waitKey(0);
       destroyAllWindows();
       close();
    }
}

