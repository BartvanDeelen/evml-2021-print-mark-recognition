import cv2
import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

import Classes.color as color
from Classes.markpattern import MarkPattern
from Classes.mark_pattern_collection import MarkPatternCollection

def get_mark_patter_collection(img, noise_filter_size):
    adjusted = np.zeros(img.shape, img.dtype)
    a = 13  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = 63
    adjusted = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    canny = cv2.Canny(blurred, 9, 14)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    dilated = cv2.dilate(canny, kernel)
    return get_mark_patterns(dilated, noise_filter_size)


# General method to get all contours in image
def get_mark_patterns(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mark_patterns = MarkPatternCollection()
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > noise_filter_size:
            mark_patterns.append(MarkPattern(contour))

    return mark_patterns


def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)


data = pd.read_csv('../data/features/markPattern_features_calibration.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

calibration_model = DecisionTreeClassifier()
calibration_model.fit(X, y)

calibration_image = cv2.imread('../data/pictures\\markPatternHighRes.png')
calibration_list = get_mark_patter_collection(calibration_image, 10000)
calibration_list.localize_mark_patterns(calibration_model)
if calibration_list.width is None:
    exit(0)

calibration_dict = dict()

for ele in calibration_list:
    calibration_dict[ele.prediction] = ele

data = pd.read_csv('../data/features/markPattern_features.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

model = DecisionTreeClassifier()
model.fit(X, y)

cam = cv2.VideoCapture(0, cv2.CAP_DSHOW)

# Check if the webcam is opened correctly
if not cam.isOpened():
    raise IOError("Cannot open webcam")

cv2.imshow('test', calibration_image)

while True:
    print('Hit enter to take photo')
    key = cv2.waitKey(0)
    print('key', key)

    if key == 13:
        result, image = cam.read()

        if image is None:
            continue

        cv2.imshow('image', image)

        markPatterns = get_mark_patter_collection(image, 15000)
        markPatterns.localize_mark_patterns(model)

        for mark in markPatterns:
            if mark.prediction in calibration_dict:
                corresponding_calibration_mark = calibration_dict[mark.prediction]
                difference = mark.position_percentage - corresponding_calibration_mark.position_percentage

                if abs(difference) > 0.001:
                    mark.draw_convex_hull(image, color.green(), 7)
                    cv2.putText(
                        image,
                        "{:.3f}".format(difference),
                        mark.StartPoint,
                        cv2.FONT_HERSHEY_COMPLEX,
                        2,
                        color.orange(),
                        2
                    )

                    cv2.imshow('image', image)
    else:
        break