import logging


class MarkPatternCollection(list):
    def __init__(self):
        self.offset_x = None
        self.offset_x2 = None
        self.width = None

    def localize_mark_patterns(self, pipeline):
        for mark in self:
            features = [mark.Angle, mark.DistanceToHull, mark.Area, mark.Corners, mark.MinEnclosingCircleRadius]
            mark.prediction = pipeline.predict([features])[0]
            
            logging.basicConfig(level=logging.INFO)
            logging.info(mark.prediction)

        self.calculate_width()

        if self.width is not None:
            for mark in self:
                mark.position_percentage = (mark.get_center_point() - self.offset_x) / self.width

    def calculate_width(self) -> None:
        ref1 = next((x for x in self if x.prediction == 'Ref 1'), None)
        ref2 = next((x for x in self if x.prediction == 'Ref 2'), None)

        if ref1 is not None and ref2 is not None:
            self.offset_x = ref1.get_left_most_point()
            self.offset_x2 = ref2.get_right_most_point()
            self.width = self.offset_x2 - self.offset_x
