import cv2
import numpy as np
from imutils.video import VideoStream
import time
from markpattern import MarkPattern
import color


def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)


# General method to get all contours in image
def get_contours(img, noise_filter_size, noise_filter_size_min):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    contour_list = []
    for contour in contours:
        area = cv2.contourArea(contour)
        perimeter = cv2.arcLength(contour, True)
        if noise_filter_size < area < noise_filter_size_min:
            contour_list.append(contour)

    return contour_list


frame_size = (320, 240)
vs = VideoStream(1, resolution=frame_size).start()
time.sleep(1.0)


def nothing(x):
    pass


cv2.namedWindow('canny')
cv2.createTrackbar('T1', 'canny', 9, 255, nothing)
cv2.createTrackbar('T2', 'canny', 16, 255, nothing)
cv2.moveWindow('canny', 1000, 10)

cv2.namedWindow('contrast')
cv2.createTrackbar('c', 'contrast', 13, 30, nothing)
cv2.createTrackbar('b', 'contrast', 53, 100, nothing)
cv2.moveWindow('contrast', 700, 10)

cv2.namedWindow('area')
cv2.createTrackbar('size', 'area', 204, 50000, nothing)
cv2.createTrackbar('min', 'area', 100000, 100000, nothing)
cv2.moveWindow('area', 700, 300)

cv2.namedWindow('bin')
cv2.createTrackbar('low', 'bin', 204, 255, nothing)
cv2.createTrackbar('high', 'bin', 255, 255, nothing)
cv2.moveWindow('bin', 700, 600)

frame = cv2.imread('pictures\\1633892946.png')

# loop over frames from the video stream
while True:
    # grab the frame from the threaded video file stream

    resized = frame.copy()

    # hsv = cv2.cvtColor(resized,cv2.COLOR_BGR2HSV)

    # yellow_low = np.array([0,0,255])
    # yellow_high = np.array([0,255,255])

    # mask = cv2.inRange(hsv,yellow_low,yellow_high)

    # resized[mask>0]=(0,0,255)
    # cv2.imshow("Frame", resized)
    # cv2.waitKey(0)

    adjusted = np.zeros(resized.shape, resized.dtype)
    a = int(cv2.getTrackbarPos('c', 'contrast'))  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = int(cv2.getTrackbarPos('b', 'contrast'))
    adjusted = cv2.convertScaleAbs(resized, alpha=alpha, beta=beta)

    kernel_sharpening = np.array([[-1, -1, -1],
                                  [-1, 9, -1],
                                  [-1, -1, -1]])

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    # test = cv2.filter2D(gray, -1, kernel_sharpening)

    # (thresh, bin) = cv2.threshold(gray, int(cv2.getTrackbarPos('low','bin')), int(cv2.getTrackbarPos('high','bin')), cv2.THRESH_BINARY)

    canny = cv2.Canny(blurred, int(cv2.getTrackbarPos('T1', 'canny')), int(cv2.getTrackbarPos('T2', 'canny')))

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11, 11))
    dilated = cv2.dilate(canny, kernel)

    size_filter = int(cv2.getTrackbarPos('size', 'area'))
    size_filter_min = int(cv2.getTrackbarPos('min', 'area'))

    contours = get_contours(dilated, size_filter, size_filter_min)

    contourimg = resized.copy()
    cv2.drawContours(contourimg, contours, -1, color.orange(), 2)

    cv2.imshow("ad", resize_image(dilated, 30))
    cv2.imshow("Frame", resize_image(contourimg, 30))
    cv2.waitKey(1)
