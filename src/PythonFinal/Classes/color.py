def green():
    return 0, 255, 0


def white():
    return 255, 255, 255


def red():
    return 0, 0, 255


def black():
    return 0, 0, 0


def purple():
    return 255, 0, 255


def orange():
    return 0, 128, 255


def blue():
    return 255, 0, 0


def yellow():
    return 0, 255, 255
