from sense_hat import SenseHat
import glob
import os
import time
from tkinter import filedialog
from imutils.video import VideoStream

import csv
import cv2
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier

from Classes.markpattern import MarkPattern
from Classes.mark_pattern_collection import MarkPatternCollection

# Function that preprocesses the images and retrieves all the marks
def get_mark_patter_collection(img, noise_filter_size):
    # Get more contrast
    adjusted = np.zeros(img.shape, img.dtype)
    a = 13  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = 63
    adjusted = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)

    # Convert to grayscale
    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    # Adaptive threshold
    thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 99, 5)

    # Remove noise
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    eroded = cv2.erode(thresh, kernel)
    dilated = cv2.dilate(eroded, kernel)

    # Get edges of marks
    canny = cv2.Canny(dilated, 9, 14)

    #  Thicken the edges
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    dilated = cv2.dilate(canny, kernel)

    return get_mark_patterns(dilated, noise_filter_size)


# General function to get all contours in image
def get_mark_patterns(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mark_patterns = MarkPatternCollection()
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > noise_filter_size:
            mark_patterns.append(MarkPattern(contour))

    return mark_patterns

# Function to resize images in %
def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)

# Read csv file with calibration features and fit calibration model
data = pd.read_csv('../data/features/markPattern_features_calibration.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

pipeline = make_pipeline(StandardScaler(), KNeighborsClassifier(n_neighbors=1))
pipeline.fit(X.values, y)

calibration_image = cv2.imread('../data/pictures/markPatternHighRes.png')
calibration_list = get_mark_patter_collection(calibration_image, 8000)
calibration_list.localize_mark_patterns(pipeline)
if calibration_list.width is None:
    exit(0)

cv2.imshow("Calibration Image", calibration_image)

calibration_dict = dict()

for ele in calibration_list:
    calibration_dict[ele.prediction] = ele

# Read csv file with features and fit the runtime model
data = pd.read_csv('../data/features/markPattern_features.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

pipeline = make_pipeline(StandardScaler(), KNeighborsClassifier())
pipeline.fit(X.values, y)

running = True

# Initialze the senseHat and videostream
sense = SenseHat()
sense.clear()

frame_size = (3280, 800)
frame_rate = 45
vs = VideoStream(src=0, usePiCamera=True, resolution=frame_size, framerate=frame_rate).start()
time.sleep(1.0)

counter = 0

# Runtime loop to take photos, predict the marks and calculate the offset.
while running:
    image = vs.read()

    # Resize and show videostream
    image_small = resize_image(image, 50)
    cv2.imshow("video", image_small)
    cv2.waitKey(50)

    if image is None:
        continue

    for event in sense.stick.get_events():
        if event.direction == 'down' and event.action == 'pressed':

            # Pre-process image, predict and localize marks
            markPatterns = get_mark_patter_collection(image, 15000)
            markPatterns.localize_mark_patterns(pipeline)

            # Compare location with calibration image and write difference to .csv file
            csv_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            for mark in markPatterns:
                if mark.prediction in calibration_dict:
                    if "Ref" in mark.prediction:
                        continue

                    corresponding_calibration_mark = calibration_dict[mark.prediction]

                    if mark.position_percentage is not None and corresponding_calibration_mark.position_percentage is not None:
                        difference = mark.position_percentage - corresponding_calibration_mark.position_percentage
                        # A pattern is 22.5 mm wide
                        difference_mm = 22.5 * difference

                        index = int(mark.prediction.replace("Press ", "", 1))
                        csv_data[index - 1] = round(difference_mm, 2)

            with open('../data/realtime/localizations.csv', 'a', newline='', encoding='UTF8') as f:
                writer = csv.writer(f, delimiter=',')
                writer.writerow(csv_data)
            
