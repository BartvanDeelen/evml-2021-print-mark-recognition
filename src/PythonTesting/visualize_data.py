import numpy as np
import color
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from matplotlib import pyplot as plt
from sklearn import tree
from os import path
from sklearn.preprocessing import LabelEncoder, StandardScaler
import seaborn as sns
from sklearn.model_selection import learning_curve

# Get pathname of the features.csv
def get_features_filename():
    basepath = path.dirname(__file__)
    filepath_data = path.abspath(path.join(basepath, "..", "data", "markPattern_features.csv"))
    return filepath_data

# Shows scatterplot of 2 features
def scatterplot(data, feature_a, feature_b):
    ax1 = sns.scatterplot(data=data, x = feature_a, y=feature_b, hue="Press code")
    ax1.set_title("Scatterplot of " + feature_a + " and " + feature_b)
    plt.tight_layout()
    plt.show()

# Shows feature distribution
def feature_distribution(title):
    ax = sns.countplot(x=y, color="skyblue")
    ax.set_title(title)
    plt.tight_layout()
    plt.show()

# Shows correlaction heatmap of all features
def corr_heatmap(title, data):
    corr = np.corrcoef(X, rowvar=False)
    ax4 = sns.heatmap(data.corr(), annot=True)
    ax4.set_title(title)
    plt.show()

# Shows histogram for a specific feature
def feature_hist(feature):
    sns.histplot(data, x=feature)
    plt.show()

# Plot desision tree
def plot_decision_tree():
    fig = plt.figure(figsize=(15,5), dpi=100)
    fn = ['Angle', 'Distance to Hull', 'Area', 'Corners', 'MinEnclosingRadius']
    cn = ['Ref 1', 'Ref 2', 'Press 1', 'Press 2', 'Press 3', 'Press 4', 'Press 5', 'Press 6', 'Press 7', 'Press 8', 'Press 9', 'Undefined']
    tree.plot_tree(model, feature_names=fn, filled=True)
    #print(tree.export_text(model))
    plt.tight_layout()
    plt.show()

# Get features csv
features_filename = 'C:\\Users\\woute\\OneDrive - HYTORC Nederland BV\\Documenten\\School\\ML\\evml-2021-print-mark-recognition\\src\\data\\features\\markPattern_features.csv'
data = pd.read_csv(features_filename)

# Prepare data
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

# Train model
model = DecisionTreeClassifier()
model.fit(X_train, y_train)

# *** FEATURES: Press code - Angle - Distance to Hull - Perimeter - Area - IsConvex - Corners - MinEnclosingRadius

# show decision tree plot
plot_decision_tree()

# show scatter plot of features a and b
scatterplot(data, "Angle", "Area")
scatterplot(data, "Angle", "Corners")
scatterplot(data, "Angle", "Perimeter")
scatterplot(data, "Angle", "Distance to Hull")
scatterplot(data, "Area", "Distance to Hull")
scatterplot(data, "MinEnclosingRadius", "Distance to Hull")
scatterplot(data, "MinEnclosingRadius", "Area")
scatterplot(data, "MinEnclosingRadius", "Corners")
scatterplot(data, "Angle", "MinEnclosingRadius")

# show correlation heatmap for a single feature
corr_heatmap("Correlation Heatmap", data.drop(columns=["IsConvex"]))
corr_heatmap("Correlation Heatmap", data.drop(columns=['Press code', 'IsConvex', 'Perimeter']))

# show feature distrubution
feature_distribution("Feature distrubution")

# show histograms of first 4 features
feature_hist("Angle")
feature_hist("Distance to Hull")
feature_hist("Area")
feature_hist("MinEnclosingRadius")
feature_hist("Perimeter")
feature_hist("Press code")
