import math
import cv2


class MarkPattern:
    # Constructor method for markPattern
    def __init__(self, img):
        self.Contour = img
        self.StartPoint = None
        self.EndPoint = None
        self.FarthestPoint = None
        self.MidPoint = None
        self.prediction = None
        self.position_percentage = None
        self.Perimeter = cv2.arcLength(self.Contour, True)
        self.Area = cv2.contourArea(self.Contour)
        self.IsConvex = cv2.isContourConvex(self.Contour)
        self.Hull = cv2.convexHull(self.Contour, returnPoints=False)
        self.defectsArray = cv2.convexityDefects(self.Contour, self.Hull)
        self.DistanceToHull = 0
        # self.ConvexHullArea = cv2.contourArea(self.Hull)
        ((x, y), self.MinEnclosingCircleRadius) = cv2.minEnclosingCircle(self.Contour)
        self.MinEnclosingCircleCenterPoint = (int(x), int(y))
        self.Box = cv2.fitEllipse(self.Contour)
        self.ConPoly = cv2.approxPolyDP(self.Contour, 0.01 * self.Perimeter, True)
        self.Corners = len(self.ConPoly)
        self.get_convex_hull_defects()
        self.Angle = self.calculate_angle()

    # Get all convexityDefects of the contour, returns an array of convexity defects.
    # The found integers are indices of the contour itself!!
    def get_convex_hull_defects(self):
        for defect in self.defectsArray:
            for s, e, f, d in defect:
                if d > self.DistanceToHull:
                    self.DistanceToHull = d
                    self.StartPoint = (self.Contour[s][0][0], self.Contour[s][0][1])
                    self.EndPoint = (self.Contour[e][0][0], self.Contour[e][0][1])
                    self.FarthestPoint = (self.Contour[f][0][0], self.Contour[f][0][1])
                    self.MidPoint = self.get_mid_point(self.Contour[s][0], self.Contour[e][0])

    # Calculate angle of convexDefect line
    def calculate_angle(self):
        angle = math.atan2(self.FarthestPoint[1] - self.MidPoint[1], self.FarthestPoint[0] - self.MidPoint[0])
        return angle * 180 / math.pi

    def get_left_most_point(self):
        x, y, width, height = cv2.boundingRect(self.Contour)
        return x

    def get_right_most_point(self):
        x, y, width, height = cv2.boundingRect(self.Contour)
        return x + width

    def get_center_point(self):
        x, y, width, height = cv2.boundingRect(self.Contour)
        return x + (width / 2)

    # Calculate midpoint between convexityDefects start and endpoint (The line from the farthest point to the midpoint
    # is the angular orientation of the mark pattern)
    @staticmethod
    def get_mid_point(a, b):
        ax = a[0]
        bx = b[0]
        ay = a[1]
        by = b[1]
        cx = (ax + bx) / 2
        cy = (ay + by) / 2
        point = (int(cx), int(cy))
        return point

    # Draw line around contour
    def draw_contour(self, img, color,thickness):
        contours = [self.Contour]
        cv2.drawContours(img, contours, 0, color, thickness)

    # Draw the minimum enclosing circle
    def draw_min_enclosing_circle(self, img, thickness, color):
        cv2.circle(img, self.MinEnclosingCircleCenterPoint, int(self.MinEnclosingCircleRadius), color, thickness)

    # Draw the approximated polygon around contour
    def draw_con_poly(self, img, thickness, color):
        polys = [self.ConPoly]
        cv2.drawContours(img, polys, 0, color, thickness)

    # Draw the convex hull perimeter
    def draw_convex_hull(self, img, color, thickness):
        hull = [cv2.convexHull(self.Contour, False)]
        cv2.drawContours(img, hull, 0, color, thickness)

    # Draw a circle around all points that are found by the convexity defects method (start, end, farthest, midpoint)
    def draw_convex_defect_all_points(self, img, radius, thickness, color):
        cv2.circle(img, self.StartPoint, radius, color, thickness)
        cv2.circle(img, self.EndPoint, radius, color, thickness)
        cv2.circle(img, self.FarthestPoint, radius, color, thickness)
        cv2.circle(img, self.MidPoint, radius, color, thickness)

    # Draw a circle around convexityDefects start point
    def draw_convex_defect_start_point(self, img, radius, thickness, color):
        cv2.circle(img, self.StartPoint, radius, color, thickness)

    # Draw a circle around convexityDefects endpoint
    def draw_convex_defect_end_point(self, img, radius, thickness, color):
        cv2.circle(img, self.EndPoint, radius, color, thickness)

    # Draw a circle around convexityDefects farthest point
    def draw_convex_defect_farthest_point(self, img, radius, thickness, color):
        cv2.circle(img, self.FarthestPoint, radius, color, thickness)

    # Draw a circle around convexityDefects midpoint
    def draw_convex_defect_mid_point(self, img, radius, thickness, color):
        cv2.circle(img, self.MidPoint, radius, color, thickness)

    # Draw a line between the convexity defects farthest and mid point (this is line is the angular orientation of the
    # mark patten)
    def draw_convex_defect_angle_line(self, img, color, thickness):
        cv2.line(img, self.FarthestPoint, self.MidPoint, color, thickness)
