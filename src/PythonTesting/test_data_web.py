import csv
import glob
import logging
import os
from tkinter import filedialog

import cv2
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier

from markpattern import MarkPattern
from mark_pattern_collection import MarkPatternCollection


def get_mark_patter_collection(img, noise_filter_size):
    adjusted = np.zeros(img.shape, img.dtype)
    a = 13  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = 63
    adjusted = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    canny = cv2.Canny(blurred, 9, 14)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    dilated = cv2.dilate(canny, kernel)
    return get_mark_patterns(dilated, noise_filter_size)


# General method to get all contours in image
def get_mark_patterns(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mark_patterns = MarkPatternCollection()
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > noise_filter_size:
            mark_patterns.append(MarkPattern(contour))

    return mark_patterns


def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)


data = pd.read_csv('../data/features/markPattern_features_calibration.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

pipeline = make_pipeline(StandardScaler(), KNeighborsClassifier(n_neighbors=1))
pipeline.fit(X, y)

calibration_image = cv2.imread('../data/pictures/markPatternHighRes.png')
calibration_list = get_mark_patter_collection(calibration_image, 10000)
calibration_list.localize_mark_patterns(pipeline)
if calibration_list.width is None:
    logging.error("no width found for calibration")
    exit(0)

calibration_dict = dict()

for ele in calibration_list:
    calibration_dict[ele.prediction] = ele

data_path = filedialog.askdirectory()
p = os.path.sep.join([data_path, '**', '*.png'])

file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]

data = pd.read_csv('../data/features/markPattern_features_cleaned.csv')
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])
y = data['Press code']

pipeline = make_pipeline(StandardScaler(), KNeighborsClassifier())
pipeline.fit(X, y)

for file in file_list:
    image = cv2.imread(file)
    markPatterns = get_mark_patter_collection(image, 15000)
    markPatterns.localize_mark_patterns(pipeline)

    cv2.imshow('image', image)

    csv_data = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
    for mark in markPatterns:
        if mark.prediction in calibration_dict:
            if "Ref" in mark.prediction:
                continue

            corresponding_calibration_mark = calibration_dict[mark.prediction]

            if mark.position_percentage is not None and corresponding_calibration_mark.position_percentage is not None:
                difference = mark.position_percentage - corresponding_calibration_mark.position_percentage
                difference_mm = 22.5 * difference

                index = int(mark.prediction.replace("Press ", "", 1))
                csv_data[index - 1] = round(difference_mm, 2)

    with open('../data/realtime/localizations.csv', 'a', newline='', encoding='UTF8') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(csv_data)

    cv2.waitKey(0)
