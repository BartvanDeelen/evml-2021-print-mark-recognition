import warnings
warnings.filterwarnings('ignore')

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import joblib
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import svm
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_curve
import pylab as pl
from matplotlib.ticker import MultipleLocator
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RepeatedStratifiedKFold
from numpy import mean
from numpy import std
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
import timeit


def show_confusion_matrix(conf_matrix, labels):
    fig = plt.figure()
    ax = fig.add_subplot()
    cax = ax.matshow(conf_matrix)
    plt.title('Confusion matrix of the classifier')
    fig.colorbar(cax)
    ax.set_xticklabels([''] + labels)
    ax.set_yticklabels([''] + labels)
    ax.xaxis.set_major_locator(MultipleLocator(1))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    plt.xlabel('Predicted')
    plt.ylabel('True')
    plt.show()

# Read data from csv
data = pd.read_csv('C:\\Users\\woute\\OneDrive - HYTORC Nederland BV\\Documenten\\School\\ML\\evml-2021-print-mark-recognition\\src\\data\\features\\markPattern_features.csv')

# Remove unused features
X = data.drop(columns=['Press code', 'IsConvex', 'Perimeter'])

# Drop results from y set
y = data['Press code']

# Scale the data
scalar = StandardScaler()
scaled_X = scalar.fit_transform(X)

# Split training and test size
X_train, X_test, y_train, y_test = train_test_split(scaled_X, y, test_size=0.2)

# Create a classifier
myClassifier = KNeighborsClassifier()       # 0.996 (0.004) --> 1.000 (0.000)
#myClassifier = DecisionTreeClassifier()    # 0.993 (0.005) --> 0.996 (0.005)
#myClassifier = svm.SVC(kernel='linear')    # 0.996 (0.004) --> 1.000 (0.000)
#myClassifier = RandomForestClassifier()    # 0.996 (0.004) --> 1.000 (0.000)
#myClassifier = LogisticRegression()        # 0.993 (0.004) --> 0.998 (0.004)

# Train classifier
myClassifier.fit(X_train, y_train)

# Make predictions
predictions = myClassifier.predict(X_test)

# plt.scatter(y_test, predictions)
# plt.xlabel("True Values")
# plt.ylabel("Predictions")
# plt.show()

# Get accuracy score
# score = accuracy_score(y_test, predictions)
# print("Accuracy score = ", score)

# Evaluate the model
cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=5, random_state=1)
scores = cross_val_score(myClassifier, scaled_X, y, scoring='accuracy', cv=cv)
print('Cross Accuracy: %.3f (%.3f)' % (mean(scores), std(scores)))

# Make confusion matrix
labels = ["Press10", "Press2", "Press3", "Press4", "Press5", "Press6", "Press7", "Press8", "Press9", "Ref1", "Ref2", "Undefined"]
conf_matrix = confusion_matrix(y_test, predictions)
#show_confusion_matrix(conf_matrix, labels)

# Make classification rerport
report = classification_report(y_test, predictions)
print(report)