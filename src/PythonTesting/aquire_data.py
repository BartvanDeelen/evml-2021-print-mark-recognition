import csv
import os
import time
import cv2
from imutils.video import VideoStream
import color
from markpattern import MarkPattern


# General method to get all contours in image
def get_contours(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contourList = []
    for contour in contours:
        area = cv2.contourArea(contour)
        perimeter = cv2.arcLength(contour, True)
        if area > noise_filter_size:
            contourList.append(contour)

    return contourList


def get_press_code(x):
    print(x)
    if x == 49:
        return "Press 1"
    if x == 50:
        return "Press 2"
    if x == 51:
        return "Press 3"
    if x == 52:
        return "Press 4"
    if x == 53:
        return "Press 5"
    if x == 54:
        return "Press 6"
    if x == 55:
        return "Press 7"
    if x == 56:
        return "Press 8"
    if x == 57:
        return "Press 9"
    if x == 122:
        return "Ref 1"
    if x == 120:
        return "Ref 2"
    else:
        return "Undefined"


def apply_brightness_contrast(input_img, brightness=0, contrast=0):
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow) / 255
        gamma_b = shadow

        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131 * (contrast + 127) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127 * (1 - f)

        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf


frame_size = (320, 240)
vs = VideoStream(1, resolution=frame_size).start()
time.sleep(1.0)

if not os.path.isfile('..\\data\\markPattern_features_calibration.csv'):
    csv_header = ["Press code", "Angle", "Distance to Hull", "Perimeter", "Area", "IsConvex", "Corners",
                  "MinEnclosingRadius"]
    with open('data\\markPattern_features.csv', 'a', encoding='UTF8') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(csv_header)

k = 0
# loop over frames from the video stream
while True:
    # grab the frame from the threaded video file stream
    frame = vs.read()

    if frame is None:
        continue

    # resize image
    scale_percent = 50
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dsize = (width, height)
    resized = cv2.resize(frame, dsize)
    resized = frame

    # Apply pre-processing filters to image
    adjusted = apply_brightness_contrast(resized, (float(9) / 10), 63)
    # cv2.imshow("Frame", adjusted)
    # cv2.waitKey(0)

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)
    # cv2.imshow("Frame", gray)
    # cv2.waitKey(0)

    # (thresh, bin) = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY)
    # cv2.imshow("Frame", bin)
    # cv2.waitKey(0)

    # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    # cv2.imshow("Frame", blurred)
    # cv2.waitKey(0)

    canny = cv2.Canny(gray, 170, 250)
    # cv2.imshow("Frame", canny)
    # cv2.waitKey(0)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    dilated = cv2.dilate(canny, kernel)
    # cv2.imshow("Frame", dilated)
    # cv2.waitKey(0)

    # Find all contours in image with an area bigger than 500px
    contours = get_contours(canny, 204)

    test = frame.copy()
    cv2.drawContours(test, contours, -1, color.orange(), 2)
    cv2.imshow("Frame", test)
    k = cv2.waitKey(1)

    # if the `q` key or ESC was pressed, break from the loop
    if k == ord("q") or k == 27:
        break
    # otherwise, check to see if a key was pressed that we are
    # interested in capturing
    elif k == 13:

        if len(contours) < 9:
            print("Error in captured image")
        else:
            with open('..\\data\\markPattern_features_calibration.csv', 'a', encoding='UTF8') as f:
                writer = csv.writer(f, delimiter=',')

                # Create empty list of markPatterns
                markPatterns = []
                for contour in contours:
                    # Create markPattern object from contour and append to list
                    mark = MarkPattern(contour)
                    markPatterns.append(mark)
                    resized_copy = resized.copy()
                    mark.draw_convex_hull(resized_copy, color.purple(), 2)

                    cv2.imshow("Frame", resized_copy)
                    pressCode = get_press_code(cv2.waitKey(0))

                    csv_data = [pressCode, mark.Angle, mark.DistanceToHull, mark.Perimeter, mark.Area, mark.IsConvex,
                                mark.Corners, mark.MinEnclosingCircleRadius]
                    writer.writerow(csv_data)
