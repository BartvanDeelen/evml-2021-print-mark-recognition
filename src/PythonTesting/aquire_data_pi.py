import csv
import glob
import os
from tkinter import filedialog
from os import path

import cv2
from matplotlib.pyplot import imshow
import numpy as np

import color
from markpattern import MarkPattern


# General method to get all mark patterns in image
def get_mark_patterns(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mark_patterns = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > noise_filter_size:
            mark_patterns.append(MarkPattern(contour))

    return mark_patterns

# Scale image to fit on screen
def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)

# Get press code from key press
def get_press_code(x):
    print(x)
    if x == 49:  # 1
        return "Press 9"
    if x == 50:  # 2
        return "Press 7"
    if x == 51:  # 3
        return "Press 5"
    if x == 52:  # 4
        return "Press 3"
    if x == 53:  # 5
        return "Press 2"
    if x == 54:  # 6
        return "Press 4"
    if x == 55:  # 7
        return "Press 6"
    if x == 56:  # 8
        return "Press 8"
    if x == 57:  # 9
        return "Press 10"
    if x == 122:  # z
        return "Ref 1"
    if x == 120:  # x
        return "Ref 2"
    else:
        return "Undefined"

# Get pathname of the features.csv
def get_features_filename():
    basepath = path.dirname(__file__)
    filepath_data = path.abspath(path.join(basepath, "..", "data", "markPattern_features.csv"))
    return filepath_data

if not os.path.isfile('../data/features/markPattern_features_calibration.csv'):
    csv_header = ["Press code", "Angle", "Distance to Hull", "Perimeter", "Area", "IsConvex", "Corners",
                  "MinEnclosingRadius"]
    with open('../data/features/markPattern_features_calibration.csv', 'a', encoding='UTF8') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(csv_header)

# Get folder with training pictures
data_path = filedialog.askdirectory(title='Select folder with training pictures')
p = os.path.sep.join([data_path, '**', '*.png'])

file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]

# Loop through all pictures in folder
for file in file_list:
    image = cv2.imread(file)
    print(file)

    adjusted = np.zeros(image.shape, image.dtype)
    a = 13  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = 63
    adjusted = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    canny = cv2.Canny(blurred, 9, 20)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    dilated = cv2.dilate(canny, kernel)

    # Find all contours in image with an area bigger than 500px
    markPatterns = get_mark_patterns(dilated, 10000)

    for mark in markPatterns:
        with open('../data/features/markPattern_features_calibration.csv', 'a', encoding='UTF8') as f:
            writer = csv.writer(f, delimiter=',')

            local_image = image.copy()

            mark.draw_contour(local_image, color.green(), 8)
            #mark.draw_convex_hull(local_image, color.green(), 8)
            mark.draw_convex_defect_start_point(local_image, 10, 5, color.red())
            mark.draw_convex_defect_end_point(local_image, 10, 5, color.black())
            mark.draw_convex_defect_farthest_point(local_image, 10, 5, color.purple())

            resized = resize_image(local_image, 30)

            cv2.imshow("Frame", resized)
            pressCode = get_press_code(cv2.waitKey(0))

            csv_data = [pressCode, mark.Angle, mark.DistanceToHull, mark.Perimeter, mark.Area, mark.IsConvex,
                        mark.Corners, mark.MinEnclosingCircleRadius]
            writer.writerow(csv_data)
