import csv
import glob
import os
from tkinter import filedialog
from os import path

import cv2
from matplotlib.pyplot import imshow
import numpy as np

import color
from markpattern import MarkPattern

from matplotlib import pyplot as plt


# General method to get all mark patterns in image
def get_mark_patterns(img, noise_filter_size):
    contours, hier = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mark_patterns = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > noise_filter_size:
            mark_patterns.append(MarkPattern(contour))

    return mark_patterns

# Scale image to fit on screen
def resize_image(img, scale):
    # resize image
    scale_percent = scale
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dsize = (width, height)
    return cv2.resize(img, dsize)

def show_histogram(img):
    histogram, bin_edges = np.histogram(img, bins=256, range=(0,255))
    plt.figure()
    plt.title("Grayscale Histogram")
    plt.xlabel("grayscale value")
    plt.ylabel("pixels")
    plt.xlim([0.0, 255.0])  # <- named arguments do not work here

    plt.plot(bin_edges[0:-1], histogram)  # <- or here
    plt.show()

# Get folder with training pictures
data_path = "C:\\Users\\woute\\HAN\\Bart van Deelen (student) - Project Vision\\Acquired Images\\20211013_TrainingPatterns"
p = os.path.sep.join([data_path, '**', '*.png'])

file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]

# Loop through all pictures in folder
for file in file_list:

    image = cv2.imread(file)
    adjusted = np.zeros(image.shape, image.dtype)
    a = 13  # Defining alpha and beta:
    alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    beta = 63
    adjusted = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

    gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    img = gray
    # global thresholding
    ret1,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
    # Otsu's thresholding
    ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    # Otsu's thresholding after Gaussian filtering
    blur = cv2.GaussianBlur(img,(5,5),0)
    ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    # plot all the images and their histograms
    images = [img, 0, th1,
            img, 0, th2,
            blur, 0, th3]
    titles = ['Original Noisy Image','Histogram','Global Thresholding (v=127)',
            'Original Noisy Image','Histogram',"Otsu's Thresholding",
            'Gaussian filtered Image','Histogram',"Otsu's Thresholding"]
    for i in range(3):
        plt.subplot(3,3,i*3+1),plt.imshow(images[i*3],'gray')
        plt.title(titles[i*3]), plt.xticks([]), plt.yticks([])
        plt.subplot(3,3,i*3+2),plt.hist(images[i*3].ravel(),256)
        plt.title(titles[i*3+1]), plt.xticks([]), plt.yticks([])
        plt.subplot(3,3,i*3+3),plt.imshow(images[i*3+2],'gray')
        plt.title(titles[i*3+2]), plt.xticks([]), plt.yticks([])
    plt.show()

    # image = cv2.imread(file)
    # adjusted = np.zeros(image.shape, image.dtype)
    # a = 13  # Defining alpha and beta:
    # alpha = float(a) / 10  # Contrast Control [1.0-3.0]
    # beta = 63
    # adjusted = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)

    # gray = cv2.cvtColor(adjusted, cv2.COLOR_BGR2GRAY)

    # show_histogram(gray)

    # bin = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY)
    # cv2.imshow("bin", bin)
    # cv2.waitKey()

    # blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    # sobelx = cv2.Sobel(blurred, cv2.CV_64F, 1, 0, borderType=cv2.BORDER_DEFAULT)
    # sobely = cv2.Sobel(blurred, cv2.CV_64F, 0, 1, borderType=cv2.BORDER_DEFAULT)

    # canny = cv2.Canny(blurred, 9, 14)

    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    # dilated = cv2.dilate(canny, kernel)

    # display_img = resize_image(sobelx,30)
    # cv2.imshow("image", display_img)
    cv2.waitKey()
