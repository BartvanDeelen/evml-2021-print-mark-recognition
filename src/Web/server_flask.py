import flask
from flask import request, jsonify
import csv

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def home():
    return open('./index.html').read()


# A route to return all of the available entries in our .
@app.route('/api/data', methods=['GET'])
def api_all():
    with open('../data/realtime/localizations.csv', newline='') as f:
        print(f)
        return jsonify(list(csv.reader(f)))

app.run(host='0.0.0.0', port = 8080, debug=True)