from http.server import BaseHTTPRequestHandler, HTTPServer
import flask

import pandas as pd

hostName = "localhost"
serverPort = 8080

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/":
            try:
                f = open('C:\Development\evml-2021-print-mark-recognition\src\Web\index.html').read()
                self.send_response(200)
                self.end_headers()
                self.wfile.write(bytes(f, 'utf-8'))
            except:
                f = "File not found"
                self.send_error(404,f)
        elif self.path == "/data":
            data = pd.read_csv('C:/Development/evml-2021-print-mark-recognition/src/data/realtime/localizations.csv', encoding='UTF8')
            self.send_response(200)
            self.end_headers()
            self.wfile.write(bytes(data.to_html(), 'utf-8'))

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")